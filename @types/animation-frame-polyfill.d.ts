
declare module "animation-frame-polyfill" {
  export const requestAnimationFrame: typeof window.requestAnimationFrame;
  export const cancelAnimationFrame: typeof window.cancelAnimationFrame;
}
