
const fs = require("fs");
const path = require("path");

const chalk = require("chalk");
const { CheckerPlugin } = require("awesome-typescript-loader");
const { Config } = require("webpack-config");

const { LoaderOptionsPlugin, DefinePlugin } = require("webpack");

const HtmlPlugin = require("html-webpack-plugin");
const ProgressBarPlugin = require("progress-bar-webpack-plugin");
const TsConfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

function createLoader(ext, use=`${ext}-loader`, base={}) {
  base.test = ext instanceof RegExp
    ? ext : new RegExp(`\\.${ext}$`);
  base.use = use instanceof Array
    ? use : [ { loader: use } ];
  return base;
}

function createStyleLoader(ext, use) {
  return createLoader(ext, [ {
    loader: "style-loader"
  }, {
    loader: "css-loader",
    options: {
      importLoaders: use.length
    }
  }].concat(use));
}

const OPTIONS =
module.exports.OPTIONS = {
  APP: require("./package.json"),
  ENV: process.env.NODE_ENV || "development",
  BUILD: path.join(__dirname, "./build"),
  SOURCE: path.join(__dirname, "./source")
};

if (!fs.existsSync(OPTIONS.BUILD)) {
  fs.mkdirSync(OPTIONS.BUILD);
}

module.exports = new Config().merge({
  target: "web",
  context: OPTIONS.SOURCE,
  mode: OPTIONS.ENV,
  entry: {
    app: OPTIONS.SOURCE
  },
  output: {
    path: OPTIONS.BUILD,
    filename: "[name].js",
    strictModuleExceptionHandling: true
  },
  node: {
    console: false,
    process: true,
    global: true,
    __filename: true,
    __dirname: true,
    Buffer: false,
    setImmediate: false
  },
  plugins: [
    new ProgressBarPlugin({
      format: chalk.yellow("[:bar] ") +
              chalk.green(":percent ") +
              chalk.cyan("(:elapseds) ") +
              chalk.grey(":msg")
    }),
    new CheckerPlugin(),
    new DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify(OPTIONS.ENV)
    }),
    new HtmlPlugin({
      title: OPTIONS.APP.name,
      filename: "index.html",
      hash: true,
      inject: "body",
      minify: { },
      meta: {
        viewport:
          "width=device-width," +
          "initial-scale=1.0," +
          "minimum-scale=1.0," +
          "maximum-scale=1.0," +
          "user-scalable=no",
        HandheldFriendly: true,
        "mobile-web-app-capable": "yes",
        "apple-mobile-web-app-capable": "yes",
        "apple-mobile-web-app-status-bar-style": "black-translucent",
        "format-detection": "telephone=no",
        "X-UA-Compatible": "IE=edge",
        "theme-color": "#4AB7AC",
        "msapplication-navbutton-color": "#4AB7AC"
      }
    })
  ],
  resolve: {
    extensions: [ ".ts", ".tsx", ".js", ".json" ],
    plugins: [
      new TsConfigPathsPlugin({
        configFile: path.join(__dirname, "./tsconfig.json")
      })
    ]
  },
  module: {
    rules: [
      createLoader("html"),
      createStyleLoader("css", [ ]),
      createLoader(/\.tsx?$/, [ {
        loader: "awesome-typescript-loader",
        options: {
          silent: true,
          configFileName: path.join(__dirname, "./tsconfig.json")
        }
      } ]),
      createLoader("js", "source-map-loader", { enforce: "pre" }),
      createLoader(/\.(gif|png|jpg|ttf|otf|eot|svg|woff2?)(\?[a-z0-9]+)?$/, [ {
        loader: "url-loader",
        options: {
          limit: Infinity
        }
      } ])
    ]
  }
}).merge((OPTIONS.ENV !== "production") ? {
  cache: true,
  profile: true,
  devtool: "#cheap-module-inline-source-map",
  devServer: {
    contentBase: OPTIONS.BUILD,
    noInfo: true,
    overlay: true,
    historyApiFallback: true
  },
  output: {
    pathinfo: true
  },
  plugins: [ new LoaderOptionsPlugin({
    debug: true,
    sourceMap: true
  }) ]
} : {
  optimization: {
    splitChunks: {
      chunks: "async",
      minSize: 1048576,
      maxSize: 2097152,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: "~",
      name: true,
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    }
  },
  plugins: [
    new LoaderOptionsPlugin({
      minimize: true
    })
  ]
});
