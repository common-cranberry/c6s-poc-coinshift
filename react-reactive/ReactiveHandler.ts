
import {
  requestAnimationFrame
} from "animation-frame-polyfill";
import {
  noop, Observable, resolve,
  Subject, Subscribable,
  Unsubscribable
} from "rxjs-poly";
import {
  publishReplay, refCount, switchMap, tap
} from "rxjs-poly/operators";

export class ReactiveHandler {
  private owner: React.Component;
  private subscriptions: Array<Unsubscribable>;

  private active: boolean;

  private requesting: boolean;
  private refreshing: boolean;
  private rendering: boolean;

  private view: React.ReactNode;
  private monitor: Subject<React.ReactNode>;
  private keepalive: Unsubscribable;

  public constructor ( owner: React.Component ) {
    this.owner = owner;
    this.subscriptions = [ ];

    this.active = false;
    this.requesting = false;
    this.refreshing = false;
    this.rendering = false;

    this.view = null;
    this.monitor = new Subject();
    this.keepalive = this.monitor.pipe(switchMap((
      node: React.ReactNode
    ): Subscribable<React.ReactNode> => {
      const injector: Observable<React.ReactNode> =
        resolve(node).pipe(publishReplay(1), refCount());
      this.subscriptions.push(injector.subscribe(noop));
      let ignore: boolean = false;
      return injector.pipe(tap(( ) => {
        if (!ignore) {
          this.clear();
          ignore = true;
        }
      }));
    })).subscribe(( node: React.ReactNode ): void => {
      this.view = node;
      if (this.active && !this.requesting && !this.rendering) {
        this.requesting = true;
        requestAnimationFrame(( ) => this.refresh());
      }
    });
  }

  public mounted ( ): void {
    this.active = true;
  }

  public unmount ( ): void {
    this.active = false;
    this.keepalive.unsubscribe();
    this.clear();
  }

  public render ( node: React.ReactNode ): React.ReactNode {
    if (!this.active || !this.refreshing) {
      this.rendering = true;
      this.monitor.next(node);
      this.rendering = false;
    }
    return this.view;
  }

  private refresh ( ): void {
    this.requesting = false;
    if (this.active) {
      this.refreshing = true;
      this.owner.forceUpdate();
      this.refreshing = false;
    }
  }

  private clear ( ): void {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
    this.subscriptions = [ ];
  }
}
