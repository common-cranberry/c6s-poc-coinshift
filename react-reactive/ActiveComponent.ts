
import * as React from "react";

import { BehaviorSubject } from "rxjs-poly";

export class ActiveComponent<P, S> extends React.Component<P, S> {
  protected active: BehaviorSubject<boolean>;

  public constructor ( p: P, s?: S ) {
    super(p, s);
    this.active = new BehaviorSubject(false);
  }

  public componentDidMount ( ): void {
    this.active.next(true);
  }

  public componentWillUnmount ( ): void {
    this.active.next(false);
  }
}
