
import * as React from "react";
import * as ReactDOM from "react-dom";

export function bootstrap ( {
  template, selector = "app-entry"
}: {
  template: React.ReactElement<any>,
  selector?: string
} ): Promise<void> {
  return new Promise(function ( accept: (( ) => void) ): void {
    const Entry: Element = document.createElement(selector);
    document.body.appendChild(Entry);
    ReactDOM.render(template, Entry, accept);
  });
}
