
import * as React from "react";
import { ReactiveHandler } from "./ReactiveHandler";
import { Constructor } from "./utils";

type ReactiveDecorator<
  T extends Constructor<React.Component>
> = ( Component: T ) => any;

export function Reactive<
  T extends Constructor<React.Component>
> ( ): ReactiveDecorator<T> {
  return function ( Component: T ): any {
    class ReactiveComponent extends Component {
      private $$handler: ReactiveHandler;

      public constructor ( ...args: Array<any> ) {
        super(...args);
        this.$$handler = new ReactiveHandler(this as any);
      }

      public componentDidMount ( ): void {
        this.$$handler.mounted();
        if (super.componentDidMount instanceof Function) {
          super.componentDidMount();
        }
      }

      public componentWillUnmount ( ): void {
        this.$$handler.unmount();
        if (super.componentWillUnmount instanceof Function) {
          super.componentWillUnmount();
        }
      }

      public render ( ): React.ReactNode {
        return this.$$handler.render(super.render());
      }
    }
    return Object.defineProperty(ReactiveComponent, "name", {
      value: `Reactive(${(Component as any).name})`
    });
  };
}
