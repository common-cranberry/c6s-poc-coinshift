
import Axios, { AxiosInstance, AxiosResponse } from "axios";

import { CoinsResponse, PricesResponse } from "./DataModels";
import { coins, prices } from "./DataPrimitives";

export class DataSource {
  private axios: AxiosInstance;

  public constructor ( ) {
    this.axios = Axios.create({
      baseURL: "https://min-api.cryptocompare.com"
    });

    this.fetchCoins();
  }

  public fetchCoins ( ): void {
    this.axios
      .get("/data/all/coinlist")
      .then(function ( { data }: AxiosResponse<CoinsResponse> ): void {
        coins.next(data);
      });
  }

  public fetchPrices (
    [ fsym, tsyms ]: [ string, Array<string> ]
  ): void {
    this.axios.get(
      `/data/price?fsym=${fsym}&tsyms=${tsyms.join(",")}`
    ).then(function ( { data }: AxiosResponse<PricesResponse> ): void {
      prices.next(data);
    });
  }
}

export const source: DataSource = new DataSource();
