
import { Observable, resolve } from "rxjs-poly";
import { flatMap, map } from "rxjs-poly/operators";

import { source } from "./DataSource";
import { fsym, refresh, tsyms } from "./InputPrimitives";

export const update: Observable<void> =
  resolve([ fsym, tsyms, refresh ])
  .pipe(flatMap(resolve), map(source.fetchPrices.bind(source)));
