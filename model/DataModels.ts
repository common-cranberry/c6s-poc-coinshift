
export type PricesResponse = {
  [key: string]: number;
};

export type Price = {
  Id: string;
  Value: number;
  Coin: Coin;
};

export type Prices = Array<Price>;

export type CoinsResponse = {
  Response: string;
  Message: string;
  BaseImageUrl: string;
  BaseLinkUrl: string;
  Data: Coins;
};

export type Coins = {
  [key: string]: Coin
};

export type Coin = {
  Id: string;
  Url: string;
  ImageUrl: string;
  Name: string;
  Symbol: string;
  CoinName: string;
  FullName: string;
  Algorithm: string;
  ProofType: string;
  FullyPremined: string;
  TotalCoinSupply: string;
  BuildOn: string;
  SmartContractAddress: string
  PreMinedValue: string;
  TotalCoinsFreeFloat: string;
  SortOrder: string;
  Sponsored: boolean;
  IsTrading: true;
};
