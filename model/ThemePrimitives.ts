
import { BehaviorSubject } from "rxjs-poly";

import { ThemesType } from "./ThemeSource";

export const theme: BehaviorSubject<keyof ThemesType> =
  new BehaviorSubject<keyof ThemesType>("light");
