
import { Observable } from "rxjs-poly";
import { map } from "rxjs-poly/operators";

import { Theme } from "@material-ui/core";
import * as primitives from "./ThemePrimitives";
import { themes, ThemesType } from "./ThemeSource";

export const theme: Observable<Theme> =
  primitives.theme.pipe(map(function ( t: keyof ThemesType ): Theme {
    return themes[t];
  }));
