
import { createMuiTheme, Theme } from "@material-ui/core/styles";

const typography: object = {
  fontFamily: "Raleway"
};

const palette: object = {
  primary: {
    main: "#0E6F79"
  },
  secondary: {
    main: "#E46B6F"
  },
  error: {
    main: "#CA323A"
  }
};

export type ThemesType = {
  light: Theme, dark: Theme
};

export const themes: ThemesType = {
  light: createMuiTheme({
    typography, palette: { ...palette, type: "light" }
  }),
  dark: createMuiTheme({
    typography, palette: { ...palette, type: "dark" }
  })
};
