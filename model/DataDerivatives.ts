
import { Observable } from "rxjs-poly";
import { map } from "rxjs-poly/operators";

import { Coins, CoinsResponse, PricesResponse } from "./DataModels";
import * as primitives from "./DataPrimitives";

export const coins: Observable<Coins> =
  primitives.coins.pipe(map(function ( cs: CoinsResponse ): Coins {
    return cs.Data;
  }));

export const prices: Observable<PricesResponse> = primitives.prices;
