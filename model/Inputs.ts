
export * from "./InputPrimitives";
export * from "./ThemePrimitives";

import { noop } from "rxjs-poly";

import { update } from "./InputDerivatives";

import { tsyms } from "./InputPrimitives";
import { theme } from "./ThemePrimitives";

import { BehaviorSubject } from "rxjs-poly";

update.subscribe(noop);

export function themeToggle ( ): void {
  theme.next(theme.value === "light" ? "dark" : "light");
}

export function addTsym ( ): void {
  tsyms.next(tsyms.value.concat(new BehaviorSubject("")));
}

export function removeTsym ( index: number ): void {
  tsyms.value.splice(index, 1);
  tsyms.next(tsyms.value);
}
