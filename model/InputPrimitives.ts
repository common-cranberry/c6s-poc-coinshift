
import { BehaviorSubject } from "rxjs-poly";

export const value: BehaviorSubject<number> =
  new BehaviorSubject(0);

export const refresh: BehaviorSubject<undefined> =
  new BehaviorSubject(undefined);

export const fsym: BehaviorSubject<string> =
  new BehaviorSubject("");

export const tsyms: BehaviorSubject<Array<BehaviorSubject<string>>> =
  new BehaviorSubject([ ] as Array<BehaviorSubject<string>>);
