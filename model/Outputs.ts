
import { BehaviorSubject, combineLatest, Observable } from "rxjs-poly";
import { map } from "rxjs-poly/operators";

import { coins, prices } from "./DataDerivatives";
import { Coin, Coins, Price, PricesResponse } from "./DataModels";
import "./DataSource";
import { fsym, tsyms, value } from "./InputPrimitives";

export * from "./DataDerivatives";
export * from "./ThemeDerivatives";
export * from "./DataModels";

export const current: Observable<Coin> =
  combineLatest(coins, fsym)
  .pipe(map(function ( [ cs, f ]: [ Coins, string ] ): Coin {
    return cs[f];
  }));

export interface Conversion extends Price {
  Source: BehaviorSubject<string>;
}

export type Conversions = Array<Conversion>;

export const conversions: Observable<Conversions> =
  combineLatest(prices, value, coins, tsyms)
  .pipe(map(function ( [ ps, v, cs, ts ]: [
      PricesResponse, number, Coins, Array<BehaviorSubject<string>>
  ] ): Conversions {
    return ts.map(function ( tsym: BehaviorSubject<string> ): Conversion {
      const Id: string = tsym.value;
      return { Id, Source: tsym, Coin: cs[Id], Value: ps[Id] * v };
    });
  }));
