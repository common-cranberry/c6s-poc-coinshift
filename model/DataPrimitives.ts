
import { ReplaySubject } from "rxjs-poly";

import { CoinsResponse, PricesResponse } from "./DataModels";

export const coins: ReplaySubject<CoinsResponse> =
  new ReplaySubject(1);

export const prices: ReplaySubject<PricesResponse> =
  new ReplaySubject(1);
