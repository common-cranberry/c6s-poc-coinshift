
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faCoins, faLightbulb, faSyncAlt, faTrashAlt
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faCoins, faLightbulb, faSyncAlt, faTrashAlt
);
