
import * as React from "react";
import { Reactive, WithMutation } from "../react-reactive";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { AppBar, Button, CssBaseline,
  MuiThemeProvider, Toolbar, Typography
} from "@material-ui/core";
import { StyleRules, Theme, withStyles } from "@material-ui/core/styles";

import * as inputs from "../model/Inputs";
import * as outputs from "../model/Outputs";

import { Converter } from "./Converter";

@WithMutation(withStyles(function ( theme: Theme ): StyleRules {
  return {
    appHeaderText: {
      marginLeft: theme.spacing.unit * 2,
      marginRight: theme.spacing.unit * 2
    },
    appHeaderSubtext: {
      opacity: 0.7
    },
    appHeaderTheme: {
      float: "right"
    }
  };
}))
@Reactive()
export class Main extends React.Component<any, any> {
  public render ( ): React.ReactNode {
    return (
      <MuiThemeProvider theme={outputs.theme as any}>
        <CssBaseline />
        <link href="https://fonts.googleapis.com/css?family=Raleway"
        rel="stylesheet" />
        <AppBar position="static" className="appHeader">
          <Toolbar>
            <FontAwesomeIcon icon={["fas", "coins"]}
              className="appHeaderIcon" />
            <Typography variant="title" color="inherit" noWrap
              className={this.props.classes.appHeaderText}>
              CoinShift
            </Typography>
            <Typography variant="title" color="inherit" noWrap
              className={this.props.classes.appHeaderSubtext}>
              Currency Converter
            </Typography>
            <Button onClick={inputs.themeToggle} color="inherit"
              className={this.props.classes.appHeaderTheme}>
              <FontAwesomeIcon icon={["fas", "lightbulb"]} />
            </Button>
          </Toolbar>
        </AppBar>

        <Converter />
      </MuiThemeProvider>
    );
  }
}
