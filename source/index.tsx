
import * as React from "react";
import "../config";
import { bootstrap } from "../react-reactive";

import { Main } from "./Main";

bootstrap({
  template: <Main compiler="Typescript" framework="React" />
});
