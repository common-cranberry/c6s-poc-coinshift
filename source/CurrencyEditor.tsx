
import * as inputs from "../model/Inputs";

import { TextField } from "@material-ui/core";
import { StyleRules, withStyles } from "@material-ui/core/styles";
import * as React from "react";
import { Reactive, WithMutation } from "../react-reactive";

@WithMutation(withStyles(function ( ): StyleRules {
  return { };
}))
@Reactive()
export class CurrencyEditor extends React.Component<any, any> {
  public render ( ): React.ReactNode {
    return (
      <span className="coinValue">
        <TextField label="Value" type="number"
          margin="dense" variant="outlined"
          onKeyUp={( e: any ): void => inputs.value.next(e.target.value)} />
      </span>
    );
  }
}
