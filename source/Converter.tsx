
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Tooltip } from "@material-ui/core";
import { StyleRules, Theme, withStyles } from "@material-ui/core/styles";
import * as React from "react";
import { of } from "rxjs-poly";
import { map } from "rxjs-poly/operators";
import { Reactive, WithMutation } from "../react-reactive";

import { CurrencyEditor } from "./CurrencyEditor";
import { CurrencySelector } from "./CurrencySelector";
import { CurrencyVisualizer } from "./CurrencyVisualizer";

import * as inputs from "../model/Inputs";
import * as outputs from "../model/Outputs";

@WithMutation(withStyles(function ( theme: Theme ): StyleRules {
  return {
    converter: {
      padding: theme.spacing.unit * 4
    },
    conversion: {
    },
    action: {
      marginTop: theme.spacing.unit * 2,
      marginRight: theme.spacing.unit * 2
    },
    divider: {
      opacity: 0.05,
      backgroundImage: `repeating-linear-gradient(-45deg,
        transparent, transparent 5px,
        ${theme.palette.text.secondary} 5px,
        ${theme.palette.text.secondary} 10px);`,
      width: "100%",
      height: theme.spacing.unit * 3,
      marginTop: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * 2
    },
    from: {
      marginTop: theme.spacing.unit * 2,
      display: "inline-block",
      marginLeft: theme.spacing.unit
    }
  };
}))
@Reactive()
export class Converter extends React.Component<any, any> {
  public render ( ): React.ReactNode {

    return (
      <div className={this.props.classes.converter}>
        <div className={this.props.classes.conversion}>
          <CurrencyEditor />
          <span className={this.props.classes.from}>
            <CurrencySelector controller={of(inputs.fsym)} />
          </span>
        </div>
        <div className={this.props.classes.divider}></div>
        <React.Fragment>
          {outputs.conversions.pipe(
          map(( ps: outputs.Conversions ): React.ReactNode => {
            return ps.map((
              p: outputs.Conversion, index: number
            ): React.ReactNode => {
              return (
                <div key={index} className={this.props.classes.conversion}>
                  <CurrencyVisualizer price={p} />
                  <CurrencySelector controller={p.Source} />
                  <Tooltip title="Remove">
                    <Button onClick={( ): void => inputs.removeTsym(index)}
                      color="secondary">
                      <FontAwesomeIcon icon={["fas", "trash-alt"]} />
                    </Button>
                  </Tooltip>
                </div>
              );
            });
          }))}
        </React.Fragment>
        <Button onClick={inputs.addTsym}
          className={this.props.classes.action}
          color="primary">
          Add Conversion
        </Button>
        <Button onClick={( ): void => inputs.tsyms.next([ ])}
          className={this.props.classes.action}
          color="secondary">
          Remove All Conversions
        </Button>
        <footer>
          <a href="https://min-api.cryptocompare.com/">
            <Button>
              Powered by CryptoCompare
            </Button>
          </a>
        </footer>
      </div>
    );
  }
}
