
import { Button } from "@material-ui/core";
import { StyleRules, Theme, withStyles } from "@material-ui/core/styles";
import * as React from "react";
import { Reactive, WithMutation } from "../react-reactive";

@WithMutation(withStyles(function ( theme: Theme ): StyleRules {
  return {
    value: {
      color: theme.palette.primary.main + " !important"
    }
  };
}))
@Reactive()
export class CurrencyVisualizer extends React.Component<any, any> {
  public render ( ): React.ReactNode {
    return (
      <React.Fragment>
        <Button disabled className={this.props.classes.value}>
          {this.props.price.Value.toFixed(4)}
        </Button>
      </React.Fragment>
    );
  }
}
