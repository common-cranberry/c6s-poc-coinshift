
import * as outputs from "../model/Outputs";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Grid, Modal, Paper,
  TextField, Tooltip, Typography } from "@material-ui/core";
import { StyleRules, Theme, withStyles } from "@material-ui/core/styles";
import * as React from "react";
import { BehaviorSubject, combineLatest,
  Observable } from "rxjs-poly";
import { map } from "rxjs-poly/operators";
import { ActiveComponent, Reactive, WithMutation } from "../react-reactive";

@WithMutation(withStyles(function ( theme: Theme ): StyleRules {
  return {
    root: {
      flexGrow: 1
    },
    title: {
      marginBottom: theme.spacing.unit * 2
    },
    modal: {
      top: "50%",
      left: "50%",
      maxWidth: "100vw",
      maxHeight: "100vh",
      transform: "translate(-50%, -50%)",
      position: "absolute",
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      overflowY: "scroll"
    },
    item: {
      color: theme.palette.text.secondary,
      boxShadow: theme.shadows[1],
      padding: theme.spacing.unit * 1
    },
    label: {
      fontWeight: "bold",
      color: theme.palette.secondary.main + " !important"
    },
    icon: {
      opacity: 0.7
    }
  };
}))
@Reactive()
export class CurrencySelector extends ActiveComponent<any, any> {
  private open: Observable<boolean>;
  private filter: BehaviorSubject<string>;

  private controller: BehaviorSubject<string>;

  private coins: Observable<outputs.Coins>;

  public constructor ( props: any, config?: any ) {
    super(props, config);
    this.controller = this.props.controller;
    this.open = combineLatest(this.controller, this.active).pipe(
      map(function ( [ next, active ]: [ string, boolean ] ): boolean {
        return active && !next;
      })
    );
    this.filter = new BehaviorSubject("");
    this.coins = combineLatest(this.filter, outputs.coins)
      .pipe(map(function (
        [ f, cs ]: [ string, outputs.Coins ]
      ): outputs.Coins {
        if (!f) { return cs; }
        f = f.toLowerCase();
        const result: outputs.Coins = { };
        for (const key in cs) {
          if (key.toLowerCase().indexOf(f) > -1) {
            result[key] = cs[key];
          }
        }
        return result;
      }));
  }

  public render ( ): React.ReactNode {
    return (
      <React.Fragment>
        <Button disabled className={this.props.classes.label}>
          {this.controller}
        </Button>
        <Tooltip title="Change Currency">
          <Button onClick={( ): void => this.controller.next("")}>
            <FontAwesomeIcon icon={["fas", "sync-alt"]}
            className={this.props.classes.icon}/>
          </Button>
        </Tooltip>
        <Modal open={this.open as any}>
          <Paper className={this.props.classes.modal}>
            <Typography variant="title" className={this.props.classes.title}>
                Select a currency
            </Typography>
            <TextField label="Search" className={this.props.classes.title}
              onKeyUp={( e: any ): void => this.filter.next(e.target.value)} />
            <Grid className={this.props.classes.root}
              container
              spacing={8}
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
            >
              {this.coins.pipe(map((
                coins: outputs.Coins
              ): React.ReactNode => {
                const list: Array<string> = Object.keys(coins);
                return list.slice(0, 25)
                .map(( key: string ): React.ReactNode => (
                  <Grid key={key} item>
                    <Button className={this.props.classes.item}
                      onClick={( ): void => this.controller.next(key)}>
                      {key}
                    </Button>
                  </Grid>
                )).concat(list.length <= 25 ? [ ] : [
                  <Grid key="_more" item>
                    <Button disabled>
                      ... {list.length - 25} more items
                    </Button>
                  </Grid>
                ]);
              }))}
            </Grid>
          </Paper>
        </Modal>
      </React.Fragment>
    );
  }
}
