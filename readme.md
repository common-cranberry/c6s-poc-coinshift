
CoinShift
===

> Currency converter

To develop
---

```sh
npm install
```

### To build

```sh
NODE_ENV=development npm run build
```

### To Release

```sh
NODE_ENV=production npm run build
```

### To Debug

```sh
npm run debug -- --port 8080
```

### To serve

```sh
npm run start
```
